import { Chain } from 'zeus';
import { userState } from '../context/store';

export const useBackend = () => {
    const { token, isLoggedIn } = userState();

    const chain = () => {
        if (token && isLoggedIn === true) {
            return Chain(process.env.NEXT_PUBLIC_HOST || '', {
                headers: {
                    Authorization: token,
                    'Content-type': 'application/json',
                },
            });
        }
        return Chain(process.env.NEXT_PUBLIC_HOST || '');
    };

    const login = async (phoneNumber: string, username: string) => {
        const response = await chain().mutation({
            login: [{ loginInput: { phoneNumber, username } }, true],
        });
        if (!response) {
            throw new Error('Something is wrong');
        }
        return response.login;
    };

    const validateUserSendOTP = async (code: string, phoneNumber: string) => {
        const response = await chain().mutation({
            validate: [{ otpInput: { code, phoneNumber } }, true],
        });
        if (!response) {
            throw new Error('Something is wrong');
        }

        return response.validate;
    };
    const postMutationWithFile = async (content: string, files: { getUrl: string; type: string }[]) => {
        const response = await chain().mutation({
            userMutation: {
                post: [{ postCreate: { content, files } }, true],
            },
        });
        if (!response) {
            throw new Error('Something is wrong');
        }
        return response.userMutation;
    };

    const uploadFiles = async (files: { name: string; type: string }[]) => {
        const response = await chain().mutation({
            userMutation: {
                uploadFiles: [{ files: files }, { getUrl: true, putUrl: true }],
            },
        });
        if (!response) {
            throw new Error('Something is wrong');
        }
        return response.userMutation?.uploadFiles;
    };

    const postMutationWithoutFile = async (content: string) => {
        const response = await chain().mutation({
            userMutation: {
                post: [{ postCreate: { content } }, true],
            },
        });

        if (!response) {
            throw new Error('Something is wrong');
        }

        return response.userMutation;
    };

    const getInfoAboutMe = async () => {
        const response = await chain().query({
            me: {
                username: true,
                createdAt: true,
                wall: { content: { content: true, files: { getUrl: true, type: true } }, createdAt: true },
            },
        });
        if (!response) {
            throw new Error('Invalid response from the backend (getInfo)');
        }
        return response.me;
    };

    const getInfoAboutUser = async (username: string) => {
        const response = await chain().query({
            getUserByUsername: [
                { userGet: { username } },
                {
                    username: true,
                    createdAt: true,
                    wall: {
                        content: { content: true, files: { getUrl: true, type: true } },
                        createdAt: true,
                    },
                },
            ],
        });

        if (!response) {
            throw new Error('Invalid response from the backend (getInfoAboutUser)');
        }
        return response.getUserByUsername;
    };

    return {
        login,
        validateUserSendOTP,
        getInfoAboutMe,
        getInfoAboutUser,
        postMutationWithFile,
        postMutationWithoutFile,
        uploadFiles,
    };
};
