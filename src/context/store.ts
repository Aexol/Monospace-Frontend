import { useEffect, useState } from 'react';
import { createContainer } from 'unstated-next';
import { useRouter } from 'next/router';

const useUserStateContainer = createContainer(() => {
    const initialStateUserName = () => {
        if (typeof window !== 'undefined') {
            const userName = String(window.localStorage.getItem('userName') || null);
            return userName;
        }
    };
    const [userName, setUserName] = useState<string | undefined>(initialStateUserName);
    const initialStateToken = () => {
        if (typeof window !== 'undefined') {
            const token = String(window.localStorage.getItem('token') || null);
            return token;
        }
    };
    const [token, setToken] = useState<string | undefined>(initialStateToken);
    const initialStatePhoneNumber = () => {
        if (typeof window !== 'undefined') {
            const phoneNumber = String(window.localStorage.getItem('phoneNumber') || null);
            return phoneNumber;
        }
    };
    const [phoneNumber, setPhoneNumber] = useState<string | undefined>(initialStatePhoneNumber);
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(true);
    const router = useRouter();

    useEffect(() => {
        if (typeof window !== 'undefined' && userName && phoneNumber && token) {
            const storedUserName = localStorage.setItem('userName', userName as string);
            const storedPhoneNumber = localStorage.setItem('phoneNumber', phoneNumber as string);
            const storedToken = localStorage.setItem('token', token as string);
        }
    }, [userName, phoneNumber, token]);

    const logOut = () => {
        setToken('');
        setUserName('');
        if (localStorage) {
            localStorage.setItem('userName', '');
            localStorage.setItem('token', '');
            localStorage.setItem('phoneNumber', '');
        }
        router.push('/');
    };

    return {
        token,
        setToken,
        phoneNumber,
        isLoggedIn,
        userName,
        setUserName,
        setPhoneNumber,
        logOut,
    };
});

export const UserStateProvider = useUserStateContainer.Provider;
export const userState = useUserStateContainer.useContainer;
