import { useState } from 'react';
import { createContainer } from 'unstated-next';

type modeModal = 'signIn' | 'otp';

const useModalStateContainer = createContainer(() => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [mode, setMode] = useState<modeModal>();

    return {
        mode,
        setMode,
        setIsOpen,
        isOpen,
    };
});

export const ModalStateProvider = useModalStateContainer.Provider;
export const modalState = useModalStateContainer.useContainer;
