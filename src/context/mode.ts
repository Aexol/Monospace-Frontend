import { useEffect, useState } from 'react';
import { createContainer } from 'unstated-next';

const useDarkModeStateContainer = createContainer(() => {
    const initialState = () => {
        if (typeof window !== 'undefined') {
            const mode = String(window.localStorage.getItem('isDarkMode') || null);
            if (mode == 'false') {
               isDarkMode = false;
               return isDarkMode;
            } else {
                isDarkMode = true;
                return isDarkMode
            }
        }
    };
    let [isDarkMode, setIsDarkMode] = useState<boolean | undefined | string>(true);
    useEffect(() => {
        if (typeof window !== 'undefined') {
            localStorage.setItem('isDarkMode', isDarkMode as string);
        }
    }, [isDarkMode]);
    return {
        setIsDarkMode,
        isDarkMode,
    };
});

export const ModeStateProvider = useDarkModeStateContainer.Provider;
export const modeState = useDarkModeStateContainer.useContainer;
