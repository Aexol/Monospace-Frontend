import { useState } from 'react';
import { createContainer } from 'unstated-next';

const useContentStateContainer  = createContainer(() => {
    const [content, setContent] = useState<string>("");

    return {
        setContent,
        content
    };
});

export const ContentStateProvider = useContentStateContainer.Provider;
export const contentState = useContentStateContainer.useContainer;
