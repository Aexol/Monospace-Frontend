import { UserStateProvider } from '../context/store';
import { ModeStateProvider } from '../context/mode';
import React from 'react';
import Post from '../components/molecules/Posts';

const Posts = () => {
    return (
        <div className={``}>
            <UserStateProvider>
                <ModeStateProvider>
                    <Post />
                </ModeStateProvider>
            </UserStateProvider>
        </div>
    );
};

export default Posts;
