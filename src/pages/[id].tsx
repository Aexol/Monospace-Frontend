import { UserStateProvider } from '../context/store';
import { ModeStateProvider } from '../context/mode';
import React from 'react';
import Post from '../components/molecules/Posts';
import { useRouter } from 'next/router';
import { ModalStateProvider } from '../context/modal';

const Posts = () => {
    return (
        <UserStateProvider>
                <ModeStateProvider>
                    <Post />
                </ModeStateProvider>
        </UserStateProvider>
    );
};

export default Posts;
