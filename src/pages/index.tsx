import { userState, UserStateProvider } from './../context/store';
import React from 'react';
import { Layout } from '../layouts';
import MainSite from '../layouts/MainSite';
import { ModalStateProvider } from '../context/modal';
import { ModeStateProvider } from '../context/mode';

const HomePage = () => {
    return (
        <UserStateProvider>
            <ModalStateProvider>
                <ModeStateProvider>
                    <MainSite />
                </ModeStateProvider>
            </ModalStateProvider>
        </UserStateProvider>
    );
};

export default HomePage;
