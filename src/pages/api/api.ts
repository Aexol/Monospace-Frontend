import { Chain } from 'zeus';
const apiUrl = process.env.NODE_PUBLIC_HOST || '';

export const api = Chain(apiUrl, {
    headers: {
        'Content-Type': 'application/json',
    },
});
