import { UserStateProvider } from '../context/store';
import React from 'react';
import WritePost from '../components/molecules/WritePost';
import { ModeStateProvider } from '../context/mode';
import { ContentStateProvider } from '../context/content';

const New = () => {
    return (
        <div className={``}>
            <UserStateProvider>
                <ModeStateProvider>
                    <ContentStateProvider>
                        <WritePost />
                    </ContentStateProvider>
                </ModeStateProvider>
            </UserStateProvider>
        </div>
    );
};

export default New;
