import '../styles/global.css';
import '../styles/phone.css'
import '../styles/toggle.css'
import '../styles/toastify.css'
import { AppProps } from 'next/app';

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
    return <Component {...pageProps} />;
};

export default App;
