import { modeState } from '@/src/context/mode';
import React, { useState } from 'react';

type PostHistoryProps = { content: string; date: string; files: any };

const PostHistory: React.FC<PostHistoryProps> = ({ content, date, files }) => {
    console.log(files);

    function isImage(file: any){
        return file.type == "image/jpeg"
    }
    
    const { isDarkMode } = modeState();
    return (
        <div className={`w-full items-center py-8`}>
            <h4 className={`mb-2 text-monospace-grey-dark`}>{date}</h4>
            <p className={`flex-wrap break-words mb-4 ${!isDarkMode ? 'text-black' : 'text-white'}`}>{content}</p>
            {files && files.filter(isImage).map((file: any)=>{
                return (
                    <img src={file.getUrl} />
                )
            })}
        </div>
    );
};

export default PostHistory;
