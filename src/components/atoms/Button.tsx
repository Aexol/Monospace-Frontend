import { modeState } from '@/src/context/mode';
import React, { MouseEventHandler } from 'react';

type ButtonProps = {
    onClick: MouseEventHandler<HTMLButtonElement>;
    className?: string;
};

const Button: React.FC<ButtonProps> = ({ children, onClick, className }) => {
    const {isDarkMode} = modeState();
    const lightStyle = 'bg-monospace-dark text-white hover:bg-monospace-grey-darkest hover:text-white';
    const darkStyle = 'bg-white text-black hover:bg-monospace-grey-light hover:text-monospace-dark';
    return (
        <button
            onClick={onClick}
            className={`${isDarkMode ? darkStyle : lightStyle} uppercase rounded-lg shadow-sm w-32 h-10 text-sm ${
                className || ''
            }`}
        >
            {children}
        </button>
    );
};

export default Button;
