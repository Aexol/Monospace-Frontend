import { DarkTheme, LightTheme } from '@/src/assets';
import { modeState } from '@/src/context/mode';
import React, { useEffect, useState } from 'react';
import Toggle from 'react-toggle';
import Switch from 'react-switch';

const ModeToggle = () => {
    const { setIsDarkMode, isDarkMode } = modeState();

    return (
        <div className={`w-full flex justify-end items-center`}>
            <Toggle
                onChange={() => {
                    setIsDarkMode(!!!isDarkMode);
                }}
                icons={{
                    unchecked: <LightTheme />,
                    checked: <DarkTheme />,
                }}
            />
            {/* <Switch onChange={toggleSwitch} checked={isDarkMode == 'true' ? true : false} /> */}
        </div>
    );
};

export default ModeToggle;
