import { AddNewFileIcon, ShareIcon } from '@/src/assets';
import { useBackend } from '@/src/backend/backend';
import { contentState } from '@/src/context/content';
import { modeState } from '@/src/context/mode';
import React, { useState } from 'react';
import Button from './Button';
import { useRouter } from 'next/router';
import { userState } from '@/src/context/store';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import axios from 'axios';
const PostFooterIcon: React.FC = () => {
    toast.configure();
    const router = useRouter();
    const { userName } = userState();
    const { isDarkMode } = modeState();
    const [selectedFile, setSelectedFile] = useState<File>();
    const [responseFile, setResponseFile] = useState('');
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [loading, setLoading] = useState(false);
    const { content } = contentState();
    const { postMutationWithFile, postMutationWithoutFile } = useBackend();
    const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files) {
            setSelectedFile(event.target.files[0]);
        }
        setIsFilePicked(true);
    };
    const notify = () => {
        toast.error('First add content or file', {
            position: 'top-center',
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: isDarkMode ? 'dark' : 'light',
        });
    };

    const handleSubmission = async () => {
        if (isFilePicked && selectedFile && content) {
            const response = await postMutationWithFile(content, ).then(
                async (response) => {
                    setLoading(true);
                    const putConfig = {
                        headers: {
                            'Content-Type': selectedFile.type,
                            'x-amz-acl': 'public-read'

                        },
                    };
                    axios
                        .put(`${response?.uploadFiles[0].putUrl}`, selectedFile, axiosConfig)
                        .then((res) => {
                            setLoading(false);
                            if (res) {
                                router.push(`/${userName}`);
                            }
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                },
            );
            console.log(response);
        } else if (content) {
            const response = await postMutationWithoutFile(content);
            if (response) {
                router.push(`/${userName}`);
            }
        }
    };

    const colorMode = isDarkMode && !selectedFile ? '#ffffff' : '#414141';
    const colorSelectedFileOrMode = selectedFile ? '#13e84b' : colorMode;
    return (
        <div className={`flex flex-wrap xl:flex-nowrap justify-center items-center w-full h-20 mt-8`}>
            <div className={` flex py-4`}>
                <div className={`p-4 xl:p-8`}>
                    <ShareIcon fill={isDarkMode ? '#ffffff' : '#414141'} />
                </div>
                <div className={`p-4 xl:p-8`}>
                    <label htmlFor="file-input">
                        <AddNewFileIcon fill={colorSelectedFileOrMode} />
                    </label>
                    <input
                        id="file-input"
                        type="file"
                        name="file"
                        onChange={changeHandler}
                        className={`hidden h-0 w-0`}
                    />
                </div>
                <div className={`p-4 xl:p-8`}>
                    <Button
                        onClick={() => {
                            handleSubmission();
                            if (content == '' && selectedFile == undefined) {
                                notify();
                            }
                        }}
                    >
                        Publish
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default PostFooterIcon;
