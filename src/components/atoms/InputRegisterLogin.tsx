import { modeState } from '@/src/context/mode';
import React, { MouseEventHandler } from 'react';
import { ChangeEventHandler } from 'react';

type InputProps = {
    onChange?: ChangeEventHandler<HTMLInputElement>;
    className?: string;
    type?: string;
    placeholder?: string;
};
const InputRegisterLogin: React.FC<InputProps> = ({ onChange, type, placeholder, className }) => {
    const {isDarkMode} = modeState();
    return (
        <input
            type={type || 'text'}
            placeholder={placeholder}
            className={`${isDarkMode ? 'bg-white text-black' : 'bg-monospace-dark text-white'} w-full px-8 rounded-xl outline-none ${className}`}
            onChange={onChange}
        />
    );
};

export default InputRegisterLogin;
