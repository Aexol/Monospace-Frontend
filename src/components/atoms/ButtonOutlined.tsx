import { modeState } from '@/src/context/mode';
import React, { MouseEventHandler } from 'react';

type ButtonProps = {
    onClick: MouseEventHandler<HTMLButtonElement>;
    className?: string;
};

const ButtonOutlined: React.FC<ButtonProps> = ({ children, onClick, className }) => {
    const { isDarkMode } = modeState();
    const lightStyle =
        'bg-white border-2 border-black text-black hover:bg-monospace-dark hover:text-white hover:border-transparent hover:text-white';
    const darkStyle = 'bg-monospace-dark border-2 border-white text-white hover:bg-monospace-grey-light hover:border-0 hover:text-black';
    return (
        <button
            onClick={onClick}
            className={`${isDarkMode ? darkStyle : lightStyle} uppercase rounded-lg shadow-sm w-32 h-10 text-sm ${
                className || ''
            }`}
        >
            {children}
        </button>
    );
};

export default ButtonOutlined;
