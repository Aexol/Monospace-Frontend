import { CaptionMonospace } from '@/src/assets';
import { modalState } from '@/src/context/modal';
import { modeState } from '@/src/context/mode';
import React from 'react';
import Button from '../atoms/Button';

const RegisterLoginRightSide: React.FC = () => {
    const { setIsOpen, setMode, isOpen } = modalState();
    const { isDarkMode } = modeState();

    return (
        <div className={`${isOpen ? 'bg-opacity-60' : ''}`}>
            <div className={``}>
                <CaptionMonospace fill={!isDarkMode ? '#414141' : '#ffffff'} />
            </div>
            <p className={`mt-12 ${!isDarkMode ? 'text-monospace-dark' : 'text-white'}`}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim.
            </p>
            <div className={`flex justify-center mt-12`}>
                <Button
                    onClick={() => {
                        setIsOpen(true);
                        setMode('signIn');
                    }}
                >
                    Sign In & Sign Up
                </Button>
            </div>
        </div>
    );
};

export default RegisterLoginRightSide;
