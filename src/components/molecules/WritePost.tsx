import { modeState } from '@/src/context/mode';
import { userState } from '@/src/context/store';
import React, { useEffect } from 'react';
import ButtonOutlined from '../atoms/ButtonOutlined';
import ModeToggle from '../atoms/ModeToggle';
import PostFooterIcon from '../atoms/PostFooterIcon';
import { useRouter } from 'next/router';
import { contentState } from '@/src/context/content';

const WritePost: React.FC = () => {
    const router = useRouter();
    const { isDarkMode } = modeState();
    const { logOut, userName, token } = userState();
    const { setContent, content } = contentState();

    useEffect(() => {
        if (token == 'null') {
            router.push('/')
        }
    }, [token]);


    return (
        <div className={`p-8 h-screen ${isDarkMode ? 'bg-monospace-dark' : 'bg-white'}`}>
            <div className={`w-full justify-between flex mb-8`}>
                <ButtonOutlined onClick={logOut}>Log Out</ButtonOutlined>
                <ButtonOutlined
                    onClick={() => {
                        router.push(`/${userName}`);
                    }}
                    className={`ml-4`}
                >
                    My posts
                </ButtonOutlined>
                <ModeToggle />
            </div>
            <div
                className={`${
                    isDarkMode ? 'bg-monospace-grey-darkest text-white' : 'bg-monospace-grey-light text-black'
                } h-3/4 p-4`}
            >
                <textarea
                    placeholder="Start here..."
                    value={content}
                    onChange={(e) => {
                        setContent(e.target.value);
                    }}
                    className={`w-full h-full ${
                        isDarkMode
                            ? 'bg-monospace-grey-darkest text-white placeholder-white'
                            : 'bg-monospace-grey-light text-black placeholder-black'
                    } outline-none text-lg resize-none`}
                ></textarea>
            </div>
            <PostFooterIcon />
        </div>
    );
};

export default WritePost;
