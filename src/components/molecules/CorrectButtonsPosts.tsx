import { userState } from '@/src/context/store';
import { useRouter } from 'next/router';
import React from 'react';
import ButtonOutlined from '../atoms/ButtonOutlined';

type CorrectButtonProps = {
    id: string | string[] | undefined;
};
/* zapytać czemu tutaj TS chce tablice stringów Artura */

const CorrectButtonsPosts: React.FC<CorrectButtonProps> = ({ id }) => {
    const router = useRouter();
    const { userName, logOut, token } = userState();
    const correctNameAndLoggedIn = userName == id && token != 'null';
    const wrongNameAndLoggedIn = userName != id && token != 'null';
    return (
        <>
            {correctNameAndLoggedIn && (
                <div className={`flex flex-col`}>
                    <ButtonOutlined onClick={logOut}>Log out</ButtonOutlined>
                </div>
            )}
            {wrongNameAndLoggedIn && (
                <div className={`flex flex-col`}>
                    <ButtonOutlined onClick={logOut}>Log out</ButtonOutlined>
                    <ButtonOutlined
                        className={`mt-4`}
                        onClick={() => {
                            router.push(`/${userName}`);
                        }}
                    >
                        My posts
                    </ButtonOutlined>
                </div>
            )}
            {token == 'null' && (
                <div className={`flex flex-col`}>
                    <ButtonOutlined
                        className={`mt-4`}
                        onClick={() => {
                            router.push(`/`);
                        }}
                    >
                        Register
                    </ButtonOutlined>
                </div>
            )}
        </>
    );
};

export default CorrectButtonsPosts;
