import { CloseIcon } from '@/src/assets';
import { modalState } from '@/src/context/modal';
import { userState } from '@/src/context/store';
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import React, { MutableRefObject, useRef, useState } from 'react';
import Button from '../atoms/Button';
import InputRegisterLogin from '../atoms/InputRegisterLogin';
import { modeState } from '@/src/context/mode';
import { useOutsideClick } from 'rooks';
import { useBackend } from '@/src/backend/backend';
import { useRouter } from 'next/router';

const Otp: React.FC = () => {
    const router = useRouter();
    const inputRef = useRef<HTMLDivElement>(null) as MutableRefObject<HTMLDivElement>;

    const { setIsOpen, setMode } = modalState();
    const { validateUserSendOTP } = useBackend();
    const { phoneNumber, setToken, userName, token } = userState();
    const [code, setCode] = useState<string>();
    const { isDarkMode } = modeState();
    useOutsideClick(inputRef, () => setIsOpen(false));
    return (
        <div
            className={`md:w-1/3 lg:w-3/12 h-1/2 top-0 left-0 ${
                isDarkMode ? 'bg-monospace-dark' : 'bg-white'
            } shadow-xl z-50 flex`}
            ref={inputRef}
        >
            <div className={`relative shadow-foodeli max-w-full h-full p-4`}>
                <div className={`flex flex-col h-full justify-between`}>
                    <div className={`flex w-full justify-end`}>
                        <button
                            className={`w-4 h-4`}
                            onClick={() => {
                                setIsOpen(false);
                            }}
                        >
                            <CloseIcon fill={!isDarkMode ? '#414141' : '#ffffff'} />
                        </button>
                    </div>
                    <h3 className={`${isDarkMode ? 'text-white' : 'text-black'} text-sm mt-4 w-full text-justify`}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident laudantium praesentium
                        officia doloribus, quasi illo autem ut itaque consequatur!
                    </h3>
                    <div className={`mb-8`}>
                        <h2 className={`${isDarkMode ? 'text-white' : 'text-black'}`}>Otp</h2>
                        <div className={`w-full flex justify-center mt-4 bg-monospace-dark h-12 rounded-xl`}>
                            <InputRegisterLogin
                                onChange={(e) => {
                                    setCode(e.target.value);
                                }}
                            />
                        </div>
                        <button
                            onClick={() => {
                                setMode('signIn');
                            }}
                        >
                            <h4
                                className={`${
                                    isDarkMode ? 'text-white' : 'text-black'
                                } text-sm mt-4 w-full text-justify`}
                            >
                                Nie otrzymałeś kodu? Kliknij tutaj
                            </h4>
                        </button>
                        <div className={`w-full flex justify-center mt-4`}>
                            <Button
                                onClick={async () => {
                                    if (code && phoneNumber) {
                                        const response = await validateUserSendOTP(code, phoneNumber).then(
                                            (response) => {
                                                try {
                                                    const newToken = response;
                                                    setToken(newToken);
                                                    router.push(`/${userName}`);
                                                } catch (error) {
                                                    console.log(error);
                                                }
                                            },
                                        );
                                    }
                                }}
                            >
                                Login
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Otp;
