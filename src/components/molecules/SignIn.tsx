import { CloseIcon } from '@/src/assets';
import { modalState } from '@/src/context/modal';
import { userState } from '@/src/context/store';
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import React, { MutableRefObject, useRef } from 'react';
import Button from '../atoms/Button';
import InputRegisterLogin from '../atoms/InputRegisterLogin';
import { modeState } from '@/src/context/mode';
import { useBackend } from '@/src/backend/backend';
import { useOutsideClick } from 'rooks';

const SignIn = () => {
    const inputRef = useRef<HTMLDivElement>(null) as MutableRefObject<HTMLDivElement>;
    const { setIsOpen, setMode } = modalState();
    const { phoneNumber, setPhoneNumber, setUserName, userName } = userState();
    const { login } = useBackend();
    const { isDarkMode } = modeState();
    useOutsideClick(inputRef, () => setIsOpen(false));
    return (
        <div
            className={`md:w-1/3 lg:w-3/12 h-1/2 top-0 left-0 ${
                isDarkMode ? 'bg-monospace-dark' : 'bg-white'
            } shadow-xl z-50 flex`}
            ref={inputRef}
        >
            <div className={`relative shadow-foodeli max-w-full h-full p-4`}>
                <div className={`flex flex-col h-full justify-between`}>
                    <div className={`flex w-full justify-end`}>
                        <button
                            className={`w-4 h-4`}
                            onClick={() => {
                                setIsOpen(false);
                            }}
                        >
                            <CloseIcon fill={!isDarkMode ? '#414141' : '#ffffff'} />
                        </button>
                    </div>
                    <h3 className={`${isDarkMode ? 'text-white' : 'text-black'} text-sm mt-4 w-full text-justify`}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident laudantium praesentium
                        officia doloribus, quasi illo autem ut itaque consequatur!
                    </h3>
                    <div className={`mb-8`}>
                        <h2 className={`${isDarkMode ? 'text-white' : 'text-black'}`}>Phone number</h2>
                        <div
                            className={`w-full flex justify-center mt-4 h-12 rounded-xl ${
                                isDarkMode ? 'bg-white text-black' : 'bg-monospace-dark text-white'
                            }`}
                        >
                            <PhoneInput
                                international
                                defaultCountry="PL"
                                placeholder="Enter phone number"
                                value={phoneNumber}
                                onChange={setPhoneNumber}
                            />
                        </div>

                        <h2 className={`${isDarkMode ? 'text-white' : 'text-black'}`}>Username</h2>
                        <div className={`w-full flex justify-center mt-4 bg-monospace-dark h-12 rounded-xl`}>
                            <InputRegisterLogin
                                onChange={(e) => {
                                    setUserName(e.target.value);
                                }}
                            />
                        </div>

                        <div className={`w-full flex justify-center mt-4`}>
                            <Button
                                onClick={() => {
                                    if (phoneNumber && userName) {
                                        login(phoneNumber, userName);
                                        setMode('otp');
                                    }
                                }}
                            >
                                Sign In
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SignIn;
