import { CutCaptionMonospace, ReadingGlass } from '@/src/assets';
import { useBackend } from '@/src/backend/backend';
import { modeState } from '@/src/context/mode';
import { userState } from '@/src/context/store';
import React, { useEffect, useState } from 'react';
import Button from '../atoms/Button';
import ModeToggle from '../atoms/ModeToggle';
import PostHistory from '../atoms/PostHistory';
import { useRouter } from 'next/router';
import CorrectButtonsPosts from './CorrectButtonsPosts';

const Posts: React.FC = () => {
    const router = useRouter();
    const routerQuery = router.query;
    const id = routerQuery.id;
    const { getInfoAboutMe, getInfoAboutUser } = useBackend();
    const [posts, setPosts] = useState(Array);
    const [isLoading, setIsLoading] = useState(false);
    const [wordEntered, setWordEntered] = useState('');
    const { isDarkMode } = modeState();
    const { isLoggedIn, userName } = userState();

    useEffect(() => {
        if (router.isReady && id) {
            try {
                const fetchData = async () => {
                    setIsLoading(true);
                    const response = await getInfoAboutUser(id as string);
                    if (response) {
                        setPosts(response.wall.reverse());
                        setIsLoading(false);
                    }
                };
                fetchData();
            } catch (error) {
                alert(error);
            }
        }
        setIsLoading(false);
    }, [router.isReady, id]);
    if (router.isReady && !isLoading) {
        return (
            <div className={`flex ${isDarkMode ? 'bg-monospace-dark' : 'bg-white'} min-h-screen`}>
                <div className={`w-1/6 p-8 hidden md:flex`}>
                    <CorrectButtonsPosts id={id} />
                </div>
                <div className={`w-full px-12 md:px-24 xl:px-32`}>
                    <div className={`w-full flex justify-center`}>
                        <CutCaptionMonospace width={'100%'} height={'100%'} fill={isDarkMode ? '#555555' : '#F2F2F2'} />
                    </div>
                    <div className={`w-full flex items-center`}>
                        <div className={`rounded-xl shadow-xl w-full h-12 p-2 flex items-center justify-between pr-4`}>
                            <input
                                onChange={(e) => {
                                    setWordEntered(e.target.value);
                                }}
                                placeholder="SEARCH"
                                className={`pl-4 w-11/12 h-8 outline-none ${
                                    isDarkMode
                                        ? 'bg-monospace-dark placeholder-white text-white'
                                        : 'bg-white placeholder-monospace-dark text-black'
                                }`}
                            />

                            <ReadingGlass />
                        </div>

                        {isLoggedIn && (
                            <Button
                                className={`ml-2 ${isLoggedIn && id == userName ? 'block' : 'hidden'}`}
                                onClick={() => router.push('/new')}
                            >
                                Add New
                            </Button>
                        )}
                    </div>
                    <div className={``}>
                        {posts &&
                            posts.map((post: any) => {
                                const day = post.createdAt.slice(8, 10);
                                const month = post.createdAt.slice(5, 7);
                                const date = `${day}.${month}`;
                                const content = post.content.content;
                                const contentLower = post.content.content.toLowerCase();
                                const files = post.content.files;
                                console.log(files)
                                if (content && contentLower.includes(wordEntered.toLowerCase())) {
                                    return (
                                        <div key={post.createdAt} className={`flex w-full`}>
                                            <PostHistory date={date} content={content} files={files}/>
                                        </div>
                                    );
                                }
                            })}
                    </div>
                </div>
                <div className={`w-1/6 p-8 hidden md:block`}>
                    <ModeToggle />
                </div>
            </div>
        );
    }

    return <h1>Loading</h1>;
};

export default Posts;
