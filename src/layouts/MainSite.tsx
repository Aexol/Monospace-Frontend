import React, { useEffect } from 'react';
import { LogoMonospace } from '../assets';
import ModeToggle from '../components/atoms/ModeToggle';
import RegisterLoginRightSide from '../components/molecules/RegisterLoginRightSide';
import SignIn from '../components/molecules/SignIn';
import { modalState } from '../context/modal';
import { modeState } from '../context/mode';
import { userState } from '../context/store';
import { useRouter } from 'next/router';
import Otp from '../components/molecules/Otp';

const MainSite: React.FC = () => {
    const router = useRouter();
    const { isOpen, mode } = modalState();
    const { token, userName } = userState();
    const { isDarkMode } = modeState();

    useEffect(() => {
        if (token != 'null' && userName != 'null') {
            router.push(`/${userName}`);
        }
    }, [token, userName]);

    return (
        <div className={`w-full h-screen ${isDarkMode ? 'bg-monospace-dark' : 'bg-white'}`}>
            <div className={`flex w-full p-8`}>
                <ModeToggle />
            </div>
            <div
                className={`flex items-center justify-center  flex-wrap lg:flex-nowrap h-5/6 ${
                    isOpen ? 'opacity-20' : 'opacity-100'
                }`}
            >
                <div className={`w-full lg:w-1/2 px-8 py-0 lg:p-8 flex justify-center`}>
                    <LogoMonospace fill={!isDarkMode ? '#414141' : '#ffffff'} />
                </div>
                <div className={`w-full lg:w-1/2 px-8 py-0 lg:p-24`}>
                    <RegisterLoginRightSide />
                </div>
            </div>
            {mode == 'signIn' && isOpen && (
                <div className={`flex fixed top-0 left-0 justify-center items-center w-full h-full`}>
                    <SignIn />
                </div>
            )}
            {mode == 'otp' && isOpen && (
                <div className={`flex fixed top-0 left-0 justify-center items-center w-full h-full`}>
                    <Otp />
                </div>
            )}
        </div>
    );
};

export default MainSite;
