import React from 'react';
import styled from '@emotion/styled';
import WritePost from '../components/molecules/WritePost';
import Posts from '../components/molecules/Posts';

export const siteTitle = 'Next.js Sample Website';

export const Layout: React.FC = () => {
    return (
        <div className={`flex`}>
            <div className={`w-3/4`}>
                <WritePost />
            </div>
            <div className={`w-1/4`}>
                <Posts />
            </div>
        </div>
    );
};
