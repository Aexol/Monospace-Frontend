module.exports = {
    purge: [
        './src/pages/**/*.{js,ts,jsx,tsx}',
        './src/components/**/*.{js,ts,jsx,tsx}',
        './src/layouts/**/*.{js,ts,jsx,tsx}',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                'monospace-orange': '#FFBB69',
                'monospace-black': '#212121',
                'monospace-grey-darkest': '#8B8787',
                'monospace-grey-dark': '#C4C4C4',
                'monospace-grey-medium': '#D8D8D8',
                'monospace-grey-light': '#F2F2F2',
                'monospace-dark': '#414141',
                'monospace-dark-warm': '#555555',
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
